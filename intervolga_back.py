from flask import Flask
import requests
from flask_cors import CORS
from flask import jsonify
import base64
import re

username = 'HackatonIntervolga2019'
password = 'coolhacker1337'


url = 'https://github.com/Aleksandr-Kuz/TestProject'
git_api_url_part1 = 'https://api.github.com/repos/'
git_api_url_part2 = '/contents/'


def find_classes_names (repo_url):
    files_mas = []

    tokens = repo_url.split('/')

    content_url = git_api_url_part1+tokens[3]+'/'+tokens[4]+git_api_url_part2

    response = requests.get(content_url, auth=(username, password)).json()

    for f in response:
        if f['name'].split('.')[-1] == 'cs':

            file_response = requests.get(content_url + f['name'], auth=(username, password))

            file_bytes = base64.b64decode(file_response.json()['content'])

            file_str = file_bytes.decode('utf-8').split('\n')

            class_names = []
            used_classes = []
            current_row = 1
            for st in file_str:
               res = re.findall(r'class',st)

               if len(res) != 0:
                   founded_classes = re.findall(r'\s*[a-z]*\s+class\s+(\D[A-Za-z_0-9]+)',st)
                   founded_parents = re.findall(r'[:,]{1}\s*(\D[A-Aa-z0-9_]+)',st)
                   if len(founded_classes) != 0:
                       class_struct = {
                           'name': founded_classes[0],
                           'string_numb': current_row,
                           'parent': founded_parents
                       }
                       class_names.append(class_struct)

               else:
                   res = re.findall(r'new', st)
                   if len(res) != 0:
                       founded_uses = re.findall(r'\s*new\s+(\D[A-Za-z_0-9]+)', st)
                       if len(founded_uses) != 0:
                            used_classes_struct = {
                                'name': founded_uses[0],
                                'string_numb': current_row
                            }
                       used_classes.append(used_classes_struct)
               current_row += 1

            file_struct = {
                'name': f['name'],
                'classes': class_names,
                'used_classes': used_classes
            }

            files_mas.append(file_struct)

    return files_mas



