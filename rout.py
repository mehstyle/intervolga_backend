from flask import Flask
from flask import request
from flask import jsonify
from flask_cors import CORS
import intervolga_back as func


app = Flask(__name__)
CORS(app)


@app.route('/get_repo_tree/', methods=['GET', 'POST'])
def main():
    return jsonify(func.find_classes_names(request.args.get('url')))

app.debug = True
app.run()